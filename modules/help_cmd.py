#!/usr/bin/env python
# -*- coding: <utf-8> -*-

"""
kookie - Just another Kute IRC Bot
file - help.py
Queries user with a list of helpful commands.

Last edit: September, 2012

Licensed under WTFPLv2 (Do What the Fuck You Want To Public License)

Version: 0.1
Works with Python 2.7, prolly not with Python 3 onwards.

Tested on Ubuntu, SuperX, Arch Linux.

http://sociallyencrypted.net <--- visit for awesome people

- DoomKookie
"""

from socket_stuff import kookie_sock

class helpEm(object):
	def __init__(self, nick):
		self.nick = nick
		self.msg1 = "--- Kookie Shop Help Counter ---"
		self.msg2 = "Commands supported: .help .md5encrypt .title .gs .lasttweet .isup .smile .insult"
		self.msg3 = "--- Usage ---"
		self.msg4 = ".help - displays help in query"
		self.msg5 = ".md5encrypt stringtoencrypt - returns MD5 of string to encrypt"
		self.msg6 = ".title site.com - returns the title of site.com"
		self.msg7 = ".gs cookies - searches google for cookies"
		self.msg8 = ".lasttweet @username - returns the last tweet of @username"
		self.msg9 = ".isup site.com - checks if site.com is up or down"
		self.msg10 = ".smile - returns a random smiley :3"
		self.msg11 = ".insult - returns a random insult"
		self.msg12 = "--- End of Help ---"
		
		self.send_help()
		
	def send_help(self):
		self.send_it = kookie_sock.send_msg_nick(self.msg1, self.nick)
		self.send_it = kookie_sock.send_msg_nick(self.msg2, self.nick)
		self.send_it = kookie_sock.send_msg_nick(self.msg3, self.nick)
		self.send_it = kookie_sock.send_msg_nick(self.msg4, self.nick)
		self.send_it = kookie_sock.send_msg_nick(self.msg5, self.nick)
		self.send_it = kookie_sock.send_msg_nick(self.msg6, self.nick)
		self.send_it = kookie_sock.send_msg_nick(self.msg7, self.nick)
		self.send_it = kookie_sock.send_msg_nick(self.msg8, self.nick)
		self.send_it = kookie_sock.send_msg_nick(self.msg9, self.nick)
		self.send_it = kookie_sock.send_msg_nick(self.msg10, self.nick)
		self.send_it = kookie_sock.send_msg_nick(self.msg11, self.nick)
		self.send_it = kookie_sock.send_msg_nick(self.msg12, self.nick)
