#!/usr/bin/env python
# -*- coding: <utf-8> -*-

"""
kookie - Just another IRC Bot
file - isup.py
Checks if a site is up/down using isup.me

Last edit: September, 2012

Licensed under WTFPLv2 (Do What the Fuck You Want To Public License)

Version: 0.1
Works with Python 2.7, prolly not with Python 3 onwards.

Tested on Ubuntu, SuperX, Arch Linux.

http://sociallyencrypted.net <--- visit for awesome people
"""


import urllib #for opening isup.me
from socket_stuff import kookie_sock

class isUpDotMe(object):
	def __init__(self, sitename):
		self.sitename = sitename
		self.up = "It\'s just you" #this is matched to check if a site is up.
		self.down = "It\'s not just you" #to check if it's down.
		self.wat = "Huh?" #to check if it exists or not.
		
		self.url = r"http://isup.me/"+self.sitename #preparing the url
		
		self.check_isup(self.url)
		
	def check_isup(self, url):
		self.u = urllib.urlopen(url).read() #opening the url
		
		if self.u.find(self.up) != -1: #if self.up is found in the webpages.
			self.msgup = "Requested Site: "+self.sitename+"   ...    Status: Site is up"
			self.send_status(self.msgup)
		elif self.u.find(self.down) != -1: #if self.down is found in the page.
			self.msgdown = "Requested Site: "+self.sitename+"   ...    Status: Site is down"
			self.send_status(self.msgdown)
		elif self.u.find(self.wat) != -1: #if self.wat is found in the page.
			self.msgerr = "Requested Site: "+self.sitename+"   ...    Error: lol Couldn't find site"
			self.send_status(self.msgerr)
			
			
	def send_status(self, msg):
		self.sstatus = kookie_sock.send_msg_chan(msg)



