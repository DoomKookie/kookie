#!/usr/bin/env python
# -*- coding: <utf-8> -*-

"""
kookie - Just another Kute IRC Bot
file - google.py
Used to make google searches through their API.

Last edit: September, 2012

Licensed under WTFPLv2 (Do What the Fuck You Want To Public License)

Version: 0.1
Works with Python 2.7, prolly not with Python 3 onwards.

Tested on Ubuntu, SuperX, Arch Linux.

http://sociallyencrypted.net <--- visit for awesome people

- DoomKookie
"""


import urllib
from socket_stuff import kookie_sock
import simplejson

class googleSearch(object):
	def __init__(self, search_term, nick):
		self.search_term = search_term
		self.nick = nick
		self.api_key = "" #Your API Key
		self.cx = "" #Your cx code
		
		#Preparing the url.
		self.url = "https://www.googleapis.com/customsearch/v1?key=%s&cx=%s:-u7mr3lo1x4&q=%s&gl=ca" % (self.api_key, self.cx, self.search_term)
		
		self.searchG(self.url, self.nick)
		
	def searchG(self, url, nick):
		try:
			self.sr = urllib.urlopen(url)
			
			#using simplejson to properly parse the results.
			self.json = simplejson.loads(self.sr.read()) 
			self.results = (self.json['items'])
			
			 
			
			for i in self.results:
				
				self.send_results("---------",nick)
				self.send_results("title: "+ i['title'].encode("utf-8"),nick)
				self.send_results("url: "+ i['link'],nick) 
				
		except:
			msg = "No results found."
			self.send_results(msg, nick)
			
	def send_results(self, string, nick):
		self.send_whis = kookie_sock.send_msg_nick(string, nick)





