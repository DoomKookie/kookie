#!/usr/bin/env python
# -*- coding: <utf-8> -*-

"""
kookie - Just another Kute IRC Bot
file - lasttweet.py
Grabs last tweet from a twitter user

Last edit: September, 2012

Licensed under WTFPLv2 (Do What the Fuck You Want To Public License)

Version: 0.1
Works with Python 2.7, prolly not with Python 3 onwards.

Tested on Ubuntu, SuperX, Arch Linux.

http://sociallyencrypted.net <--- visit for awesome people

- DoomKookie
"""


import twitter #python-twitter API.
from socket_stuff import kookie_sock

class twatSan(object):
	#I thought of naming it something else, but Twat-san sounded so cute ;3
	def __init__(self, username):
		self.username = username
		self.get_last_tweet(self.username)
		
	def get_last_tweet(self, username):
		self.twit = twitter.Api() #calling the API
		
		try:
			self.timeline = self.twit.GetUserTimeline(username)
			self.tweets = [s.text for s in self.timeline]
			self.ltweet = "Last tweet by @"+username+": "+ self.tweets[0].encode('utf-8')
			self.send_tweet(self.ltweet)
			
		except:
			self.ntweet = "No tweets found for @"+username+". Sowie :3"
			self.send_tweet(self.ntweet)
			
	def send_tweet(self, tweet):
		self.sendtwit_to_twat = kookie_sock.send_msg_chan(tweet)
		
		
	




