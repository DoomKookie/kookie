#!/usr/bin/env python
# -*- coding: <utf-8> -*-

"""
kookie - Just another IRC Bot
file - encryption.py
Creates MD5 of given string

Last edit: September, 2012

Licensed under WTFPLv2 (Do What the Fuck You Want To Public License)

Version: 0.1
Works with Python 2.7, prolly not with Python 3 onwards.

Tested on Ubuntu, SuperX, Arch Linux.

http://sociallyencrypted.net <--- visit for awesome people
"""

import hashlib #for MD5 encryption
from socket_stuff import kookie_sock #for sending messages to channel

class md5encrypt(object):
	def __init__(self, string):
		self.md5ec = hashlib.md5(string).hexdigest()
		self.msg = "MD5 hash for "+string+" is: "+self.md5ec
		self.send_it(self.msg)
		
	def send_it(self, msg):	
		self.send_the_msg = kookie_sock.send_msg_chan(msg)



	
