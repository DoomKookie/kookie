#!/usr/bin/env python
# -*- coding: <utf-8> -*-

"""
kookie - Just another Kute IRC Bot
file - default_conf.py
Keeps the server, nick, channel, etc. configurations

Last edit: September, 2012

Licensed under WTFPLv2 (Do What the Fuck You Want To Public License)

Version: 0.1
Works with Python 2.7, prolly not with Python 3 onwards.

Tested on Ubuntu, SuperX, Arch Linux.

http://sociallyencrypted.net <--- visit for awesome people

- DoomKookie
"""


#Default Configurations are set.
#Please change them to your own settings. <3
#Feel free to drop in on our channel for a nice chat some time :3

server = "irc.tddirc.net"
port = 6667
channel = "#se"
nick = "kookie"
ident = "k00kiest0rm"
realname = "k00kiest0rm"
