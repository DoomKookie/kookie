#!/usr/bin/env python
# -*- coding: <utf-8> -*-

"""
kookie - Just another Kute IRC Bot
file - pong.py
Replies to server PINGs with PONGs

Last edit: September, 2012

Licensed under WTFPLv2 (Do What the Fuck You Want To Public License)

Version: 0.1
Works with Python 2.7, prolly not with Python 3 onwards.

Tested on Ubuntu, SuperX, Arch Linux.

http://sociallyencrypted.net <--- visit for awesome people

- DoomKookie
"""


#imports the kookie_sock object variable for using the same class instance.
#it is used to call the send_msg_server method to send the PONG to the
#server
from socket_stuff import kookie_sock

class pong_irc(object):
	def __init__(self, ircmsg):
		self.ircmsg = ircmsg
		"""
		A PING request looks like:
		PING :ir.uk.tddirc.net
		
		So, to make sure that we are encountering a PING from the server
		we must check the first item of the splitted list, which is at
		index zero (0). We'll compare it using the find() function. find()
		returns -1 if it doesn't find anything. 
		"""
		if self.ircmsg.split()[0].find("Ping") != -1 or self.ircmsg.split()[0].find("PING") != -1:
			self.send_pong(self.ircmsg) #calls send_pong. might seem a bit unpythonic but meh.
	
	def send_pong(self, msg):
		self.servername = msg.split()[1] #The server that we'll be replying to
		#is the second item of the list, with index 1. 
		
		m = "PONG" #we need to send PONG literally :P just setting it up here.
		pong = kookie_sock.send_msg_server(m,str(self.servername)) 
		#we finally use
		#kookie_sock to call the send_msg_server method in socket_stuff.py to
		#send the PONG to the server.
	
	
	

			
			
	
		
		
			
