#!/usr/bin/env python
# -*- coding: <utf-8> -*-

"""
kookie - Just another Kute IRC Bot
file - insult.py
Randomly outputs an insult.
Special thanks to the site:
http://randominsults.net

Last edit: September, 2012

Licensed under WTFPLv2 (Do What the Fuck You Want To Public License)

Version: 0.1
Works with Python 2.7, prolly not with Python 3 onwards.

Tested on Ubuntu, SuperX, Arch Linux.

http://sociallyencrypted.net <--- visit for awesome people

- DoomKookie
"""


from socket_stuff import kookie_sock
import urllib #for opening randominsults.net
from bs4 import BeautifulSoup #for parsing and extracting the required string


class throwInsult(object):
	def __init__(self):
		self.url = "http://randominsults.net"
		self.do_insult(self.url)
		
	def do_insult(self, url):
		self.u = urllib.urlopen(url).read()
		self.soup = BeautifulSoup(self.u)
		
		self.insult = self.soup.html.body.strong.i.string
		self.insult = self.insult.encode("utf-8")
		
		self.send_insult(self.insult)
		
	def send_insult(self, insult):
		self.send_ins = kookie_sock.send_msg_chan(insult)
		
	
