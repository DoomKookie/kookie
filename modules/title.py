#!/usr/bin/env python
# -*- coding: <utf-8> -*-

"""
kookie - Just another Kute IRC Bot
file - title.py
Grabs title for a given webpage

Last edit: September, 2012

Licensed under WTFPLv2 (Do What the Fuck You Want To Public License)

Version: 0.1
Works with Python 2.7, prolly not with Python 3 onwards.

Tested on Ubuntu, SuperX, Arch Linux.

http://sociallyencrypted.net <--- visit for awesome people

- DoomKookie
"""


'''
StreetMentioner showed an interesting thing with the .title command.
Since, it gets titles from any website, it can be used to get titles
from sites that display IP addresses in the title tags :P which
means someone can issue that command and get the ip address your 
computer is running. If you're on a channel, with l3337 hax0rs who 
wanna blow up IPs on their very sight, I highly suggest disabling this
feature for your safety. Else, I guess it's just fine :3 I might add
a filter for such sites later. 

Another solution is to run it on a different computer or from
a shell account and so on.
'''







import urllib #for opening the URL
from socket_stuff import kookie_sock #for sending messages to channel


class findTitle(object):
	def __init__(self, string, tag1, tag2):
		try:
			#Searches for text between <title> tags.
			self.string = urllib.urlopen(string).read()
			self.search = self.string.find(tag1)
			self.search2 = self.string.find(tag2,self.search)
			try: #if all goes well...
				self.title = "Title for requested webpage: "+str(self.string[self.search+len(tag1):self.search2].strip())
				self.send_title(self.title)
			except: #if something fails...
				self.title = "No title found for requested webpage."
				self.send_title(self.title)
		except:
			self.msg = "No title found for requested webpage."
			self.send_title(self.msg)
			
		
	
	def send_title(self, msg):
		self.send_it = kookie_sock.send_msg_chan(msg)
		
		
			

