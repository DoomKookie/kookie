#!/usr/bin/env python
# -*- coding: <utf-8> -*-

"""
kookie - Just another Kute IRC Bot
file - command_parser.py
Properly processes the user commands

Last edit: September, 2012

Licensed under WTFPLv2 (Do What the Fuck You Want To Public License)

Version: 0.1
Works with Python 2.7, prolly not with Python 3 onwards.

Tested on Ubuntu, SuperX, Arch Linux.

http://sociallyencrypted.net <--- visit for awesome people

- DoomKookie
"""


from encryption import md5encrypt #for the md5 encryption function
from title import findTitle #for the site title function
from google import googleSearch #for the google search function
from lasttweet import twatSan #for the last tweet function
from isup import isUpDotMe #for the isup.me function
from help_cmd import helpEm #for the help function
from smile import smileAtEm #for the random smiley function
from insult import throwInsult #for the random insult function


class commandParser(object):
	def __init__(self, ircmsg):
		self.ircmsg_ = ircmsg
		self.splt = self.ircmsg_.split() #creates the list by splitting
		self.commands() #calls the next method immediately
		
	
	def commands(self): #checks for commands in the 3rd list index.
		#md5encrypt
		if self.splt[3].find(".md5encrypt") != -1: #here, .md5encrypt is the command.
				
				self.hstring = ' '.join(self.splt[4:]) #If there are more than one words in 
				#the string to be encrypted, it is joined with the join() function with
				#a space in between them. split[4:] means from the 4th to last index.
				
				self.md5e(str(self.hstring)) #calls md5e method below. 
				
		#Title
		
		if self.splt[3].find(".title") != -1:
			if self.splt[4].find("http") != -1:  
				self.uTitle(str(self.splt[4]))
				#if "http://" is already entered with the link, uTitle()
				#is called directly. Remember that 4th index is now the
				#site name.
				
			else:
				self.splt[4] = "http://"+self.splt[4]
				#if "http://" is not provided, it is added here.
				self.uTitle(str(self.splt[4]))
					
					
		#Google Search
		if self.splt[3].find(".gs") != -1:
			self.slist = self.splt[4:]
			self.query = '+'.join(self.slist[0:]) 
			#search query is of the form: how+to+make+cookies
				
			#The nick is in the 0th index of the list between a
			#semi colon and exclamation mark. It is extracted using
			#find().
			self.n1 = self.splt[0]
			self.nick = self.n1[self.n1.find(":")+1:self.n1.find("!")]
			
			self.do_search(self.query, self.nick)
				
				
		#Twitter: Last Tweet 
		if self.splt[3].find(".lasttweet") != -1:
			self.username = self.splt[4].strip("@") #The @ symbol is removed.
			self.fetch_tweet(self.username)
				
			
		#Isup.me
		if self.splt[3].find(".isup") != -1:
			self.sname = self.splt[4]
			if self.sname.find("http://") != -1: 
				self.sname = self.sname[7:] #"http://" is removed.
				self.site_up(self.sname)
				
			elif self.sname.find("https://") != -1:
				self.sname = self.sname[8:]  #"https://" is removed.
				self.site_up(self.sname)
			else:	
				self.site_up(self.sname)
				
		
		#No args commands
		
		#Help 
		if self.splt[3].find(".help") != -1:
			self.n = self.splt[0]
			self.nickname = self.n[self.n.find(":")+1:self.n.find("!")]
			#help is queried to the person, so the nick is required.
			self.help_the_guy(self.nickname)
			
		#Smile	
		if self.splt[3].find(".smile") != -1:
			self.smile_at()
				
		#Insult
		if self.splt[3].find(".insult") != -1:
			self.insult_kill()	
			
			
	def md5e(self, string):
		self.mobj = md5encrypt(string) #calls md5encrypt class in encryption.py.
		
	def uTitle(self, string): 
		self.obj = findTitle(string, "<title>", "</title>") #calls findTitle in title.py
		
	#Basically, they all call their respective classes from their files.	
	def do_search(self, query, nick):
		self.sobj = googleSearch(query, nick)
		
	def fetch_tweet(self, username):
		self.tobj = twatSan(username)
		
	def site_up(self, site):
		self.statobj = isUpDotMe(site)
		
	def help_the_guy(self, nick):
		self.hobj = helpEm(nick)
		
	def smile_at(self):
		self.smileobj = smileAtEm() 
		
	def insult_kill(self):
		self.insobj = throwInsult()
			
			
		
