#!/usr/bin/env python
# -*- coding: <utf-8> -*-

"""
kookie - Just another Kute IRC Bot
file - smile.py
Outputs a random smiley.

Last edit: September, 2012

Licensed under WTFPLv2 (Do What the Fuck You Want To Public License)

Version: 0.1
Works with Python 2.7, prolly not with Python 3 onwards.

Tested on Ubuntu, SuperX, Arch Linux.

http://sociallyencrypted.net <--- visit for awesome people

- DoomKookie
"""

from socket_stuff import kookie_sock
from random import choice #or having a random choice

class smileAtEm(object):
	def __init__(self):
		self.eyes_list = ['q','Q','w','W','e','T','u','U','o','O','a',';',"'",'"','x','X','c','C','v','V','n','m','<','>','.','?','!','@','$','^','*','-','=','+']
		self.mouth_list = ['_','.']
		
		self.eye = choice(self.eyes_list) #choose a random character from eyes_list
		self.mouth = choice(self.mouth_list) #choose a random character from mouth_list
		
		self.smile(self.eye, self.mouth)
		
	def smile(self, eye, mouth):
		self.sm = str(eye+mouth+eye)
		self.smile_send = kookie_sock.send_msg_chan(self.sm)
