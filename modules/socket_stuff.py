#!/usr/bin/env python
# -*- coding: <utf-8> -*-

"""
kookie - Just another IRC Bot
file - socket_stuff.py
Contains every method related to sockets.

Last edit: September, 2012

Licensed under WTFPLv2 (Do What the Fuck You Want To Public License)

Version: 0.1
Works with Python 2.7, prolly not with Python 3 onwards.

Tested on Ubuntu, SuperX, Arch Linux.

http://sociallyencrypted.net <--- visit for awesome people
"""


import socket #for using sockets of course xD
import default_conf #this is where the server, nick, etc. is stored.




	
class sockety_cookie(object):
	def kookie_connect(self): #This method basically connects to the server
		import time
		self.ircsock = socket.socket()
		self.ircsock.connect((default_conf.server, default_conf.port))
		self.ircsock.send("USER %s %s bla :%s\r\n" % (default_conf.ident, default_conf.server, default_conf.realname))
		self.ircsock.send("NICK "+ default_conf.nick +"\n")
		time.sleep(5)
		
		self.join_chan()
				
	def sock_irc(self): #For receiving messages from the server
		self.ircmsg = self.ircsock.recv(2048)
		return self.ircmsg
		
	def join_chan(self): #This is just for joining the channel.
		self.ircsock.send("JOIN "+ default_conf.channel +"\n")
		
		
	def send_msg_server(self,msg,server): #For sending messages to the server, like PONG, etc.
		self.ircsock.send(msg+" "+server+"\n")
	
	
	def send_msg_chan(self, a_msg): #For sending messages to the channel.
		self.ircsock.send("PRIVMSG "+ default_conf.channel + " :"+a_msg+"\n") 
		
	def send_msg_nick(self,msg, nick): #For querying other users.
		self.ircsock.send("PRIVMSG "+nick+" :"+msg+"\n") 
		
		

			
				
#Creating an object out of this class. We'll use this object in 
#every other file, so that we actually have the same instance
#of the class :3
kookie_sock = sockety_cookie()
			

		

	

