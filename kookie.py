#!/usr/bin/env python
# -*- coding: <utf-8> -*-

'''
kookie - Just another Kute IRC Bot
file - kookie.py

Last edit: September, 2012

Licensed under WTFPLv2 (Do What the Fuck You Want To Public License)

Version: 0.1
Works with Python 2.7, prolly not with Python 3 onwards.

Tested on Ubuntu, SuperX, Arch Linux.

http://sociallyencrypted.net <--- visit for awesome people

- DoomKookie
'''

"""
Usage:
Open the modules folder and set up your server, port, nick, etc. in 
default_conf.py.

To use the google search function, you must have a Google Custom Search
API key and a CX code. Both are available for free but with a 100 search
queries limit per day on Google's API.

Warning: This version is very crude. It does not even support 
server and nick authentication. These will be added later. Sorry ;3

Read the README.txt file for more information.

After everything's set up, run:
python kookie.py
"""

"""
Feel free to join us at our IRC --
Server: irc.sociallyencrypted.com
Port: 6667
Channel: #se
"""

#The following modules are imported from the "modules" folder.
#Make sure th empty __init__.py is present in that folder.
#It ensures that the folder acts as a package.

from modules.socket_stuff import kookie_sock #for the socket connections 
from modules.pong import pong_irc #for replying server PING with PONG
from modules.command_parser import commandParser #basically, processes all the commands


"""
Word of Caution before proceeding.
Be prepared to observe wildly, badly written code. This was just a learning
thing for me, so don't expect anything awesome. Plus, I have little to no 
idea on object orientation, so, I apologize for the upcoming nasty classes
and methods which are not "pythonic". It just works...


Now, about the object variable, kookie_sock. It's actually an instance of
the sockety_cookie class defined in socket_stuff.py. I had it initialized
in socket_stuff.py itself and imported it into all the other files, so that
they use the same instance of the class.
"""


class kookieBot(object):
	def __init__(self):
		self.start = kookie_sock #the object variable defined in socket_stuff.py
		self.connect = self.start.kookie_connect() #connecting to the server and channel.
		#Please check socket_stuff.py for the kookie_connect() method. 
		
	def main(self):
		while 1:
			self.ircmsg = self.start.sock_irc() #sock_irc() basically is what we receive from the server.
			self.ircmsg = self.ircmsg.strip('\n\r') #stripping any newlines and unnecessary characters.
			print(self.ircmsg) #prints the received message from the server.
			
			#Pong
			self.ponger(self.ircmsg) #calls the ponger() method defined below.
			#So, what this does is, it replies each PING from the server with 
			#a PONG. So the server knows we are alive. The ponger method calls
			#methods in a class defined in another file, pong.py.
			
			
			
			#The following uses the split() function. It splits whatever
			#arguments it is provided into a list. Here, we are splitting
			#the server reply. It will contain a command that we'll have to
			#check only if this list has at least 4 items.
			if len(self.ircmsg.split()) >= 4: 
				#command parser
				kookie_commands = commandParser(self.ircmsg) 
				#This calls the class commandParser defined in command_parser.py,
				#which basically processes and checks for the commands.
				
			else: #If the no. of elements in that list is less than 4, we want it to do nothing,
				  #since no commands have to be processed (commands appear at the 3rd index of the
				  #list, which means length of the list must at least be 4).
				pass
				
			
	def ponger(self, msg): #method that calls pong_irc from pong.py to send PONG to the server.
		pong = pong_irc(msg)
		
	
			

if __name__=="__main__": #Although, this is mainly done in GUI programming, it works here too I guess xD
	kookie = kookieBot() #creates an object for the kookieBot() class, and starts the bot with the __init__ constructor.
	main_loop = kookie.main() #calls the main() method in the kookieBot() class, and keeps the bot running.

	
