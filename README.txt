"""
kookie - Just Another Kute IRC Bot
file - README.txt
Well, ye know, read it xD

Last edit: September, 2012

- DoomKookie
"""

-----
Introduction
-----

kookie is just a learning project for me. I haven't been in touch with 
my Python skills (lol, skills xD) for a while and this is what I decided
to come up with. I originally followed the IRC Bot making tutorial in
Shellium.org, but then decided to extend the bot just for fun and 
experience. If you want the guide at Shellium.org, please follow this
link: http://wiki.shellium.org/w/Writing_an_IRC_bot_in_Python

It's a short but nicely written guide. I also wanted to try out
Object Oriented Programming in Python. I honestly don't understand why
people like OO so much. It was quite the headache...or maybe I just suck
at properly understanding it. Either way, I might upload the original
single-file version of this bot, once I fix the bugs over there, which 
of course, I have fixed in this version.


-----
Setting Up the Bot
-----

Ok, if you tried running the bot directly, you might find that it doesn't
work directly :P Well, you need to set up some things first. Firstly,
you'd want to make sure it connects to the correct server. All the server,
nick, realname and all that jazz are stored in the default_conf.py file.

Go inside the "modules" folder and open default_conf.py. Change those
settings to the ones you want.

BTW, kookie doesn't support server and nick authentication, yet. Sorry, 
I'll try adding them in the next version, k? <3

Here, I'd like to say that you should go and install all the dependencies
as well. kookie is sadly dependent on many other python packages which
don't ship in officially. Things you'd totally need are:
[*] python-twitter (http://code.google.com/p/python-twitter/)
[*] simplejson (http://pypi.python.org/pypi/simplejson/)
[*] BeautifulSoup4 (http://www.crummy.com/software/BeautifulSoup/)

You can install most of them by untaring them, cd'ing into the untarred
directory and then running (may need root permissions):
[*] python setup.py build
[*] python setup.py install

Haven't tried this on Windows, sorry :P If it works, tell me, I'll add 
your name to the changelog and stuff and give you credits and a cookie.


Next thing, you'd want to make sure that Google Search works. For this,
you'll need to get yourself an API key for the Custom Search Engine and
a CX code. Both are available in Google's API page.

Go here: http://code.google.com/apis/console

Read the instructions there, get the API key and be a boss. You'll also
need a cx code. I couldn't find it there (cuz I'm so fudging dumb). So,
I just went ahead and made a custom search engine using google from their
site and such. There I got a cx code xD Go, try it and be a boss <3

Once you get the API key (keep that a secret, kay? <3) and the cx code,
open the file "google.py" inside the "modules" folder. Inside the class,
in the __init__ constructor, you'll see two empty variables.

self.api_key: This is where you set your own API Key. Make sure you enter
your key within quotes.

self.cx: This is where you set the cx code from the custom search engine.
Again, put it in quotes.

So, yeah that's it for setting up the bot.

Note: If you don't give a flying fudge about google search, just comment
out the lines related to the command ".gs" in the command_parser.py file
inside "modules" folder.

Once everything is set, you can run kookie.py as:
[*] python kookie.py (if you have Python 2.7)
					or
[*] python2 kookie.py (if you have Python 3 installed along with 
						Python 2.7)
						
						
-----
kookie Commands :3
-----

kookie is still a lil babie and doesn't support many commands. Here are
the ones that she can understand.


.help 
- Queries the command issuer with basic help regarding commands

.md5encrypt stringtoencrypt
- returns the MD5 of stringtoencrypt.

.title site.com
- returns the title of the webpage at site.com

.smile
- Returns a random japanese smiley ^_^

.gs how to make cookies
- searches google for "how to make cookies" and queries you the first
  10 results.
  
.lasttweet @username
- returns the last tweet by @username

.isup site.com
- checks if site.com is up or down

.insult
- Returns a random insult back :P This is my personal fav.


----- 
Files, functions and stuff
-----

kookie makes use of some functions like find() and split() extensively
to process the commands. 

A usual IRC reply from server when a user types a command looks like:
:DoomKookie!hcbchdbcjhbc@bdcdbcdb.IP PRIVMSG #se :.gs cookies

if we apply split() to it, we get the following list in python:
s = [':DoomKookie!hcbchdbcjhbc@bdcdbcdb.IP', 'PRIVMSG', '#se', '.gs', 'cookies']

So now, the command name is .gs. We need to use find() on the 3rd index
of the list to search if the user has typed the command. For example,
here we'd go something like:

if s[3].find(".gs") != -1:
	...
	...
	...

and so on...

So basically that's how the commands are parsed. The commands which take
arguments, for them, you'll need to use the 4th index of that list.

Next, check the kookie.py file.

You'll see that commandParser is called only when the length of the list
is greater than or equal to 4. This is because often the bot will see server
requests of things which when splitted don't have a 3rd index, which means
there is no user input, hence, it doesn't need to go through commandParser.

I originally made the mistake by putting it as greater than or equal to 3.
Confused it with 3rd index and shizz. Throws out an out of range index error
and crashes the bot if someone quits or changes nick xD Because these replies
don't have 4 items in the list when splitted, threby no 3rd index, and so the
program tries to compare an item which doesn't exist and so it crashes. Phew,
that was a mouthful xD


Now, let's get to the "modules" folder.

socket_stuff.py contains all the stuff related to scokets -- connecting,
joining channels, sending messages, etc.

pong.py is responsible for sending PONG requests to the server, when they
send a PING request to kookie to check if she's alive.

encryption.py contains code for the md5 encryption of the given string.
I plan on extending it for sha1 and other stuff as well later.

title.py contains the method for pulling out everything between two given
strings. We pass it the page read by urllib.urlopen(url).read() and the
<title>, </title> tags, so it pulls the title from the webpage that was read
and pulls out the string between the title tags.

default_conf.py, well it contains the default server configurations. Change
it as you see fit for your use.

help_cmd.py contains code for sending the help messages to the user.

smile.py contains the code for doing those random smileys.

google.py basically connects to google's api and does a custom search on
the provided string. Then it send the user 10 results.

lasttweet.py makes use of the python-twitter package and uses twitter's api
to grab the last tweet from a user.

isup.py makes use of the site http://isup.me to check if a given site is 
up or down. It makes use of text strings to do the comparisons.

insult.py contains the code that connects to http://randominsults.net, grabs
the insult there using BeautifulSoup and then returns it to the channel.

command_parser.py, well it does all the command parsing. It's a friggin
hell with all those statements. It's sad how python does not have a 
switch case statement to make things look more beautiful and efficient.


__init__.py, this is a blank file to ensure that the folder acts as
a package.


Ok, now one directory above "modules", you should see kookie.py. This is the
file that you run. It has a class that basically calls the object variable
made in socket_stuff.py to connect to the channel and stuff. Then it has a method,
main() which has a while loop that keeps on running. It checks for PING 
requests from the server and also calls the commandParser class if the 
length of the splitted list discussed earlier is greater than or equal to 4.

And, you should also see a file called CHANGELOG.txt. It basically describes
the changes that I have made to kookie over time. If you like keeping track,
you might want to check that baby out.

TODO.txt basically lists the things I have in mind and want to complete.
Feel free to pick something up, complete it and send me a request to add
the changes.

Lastly, DEPENDS.txt lists the stuff that kookie is dependent on. You need to
have these installed in order for cookie to run properly.



------
Creating New Modules
------

If you're creating a new command module for kookie, make sure you do it this way.

[*] Add an if statement regarding finding the command in command_parser.py
[*] Make sure the if statement calls another method after the variables are set.
[*] The method that's called from the if statement should initiate the class/method 
	from your main module file.
[*] Lastly, make the module file do what it's supposed to do.

I know this isn't very pythonic but meh, it works so whatever. If this isn't clear,
check the source (especially command_parser.py) and see how it's being done :3

A little RFC that might come in handy when working with IRC:
http://www.kvirc.de/docu/doc_rfc2812.html


------
Conclusion
------

kookie is just a baby project that I'm using to learn Python better. If you
plan on using it, I respect your courage. Because it's definitely about to
crash a lot xD

While creating kookie, I have learnt a lot of new stuff. I was dead scared
of string manipulation and now I believe it's pretty much cured :3

I also learnt something about sockets and a LOT about how IRC works. ^o^

So, yeah, all-in-all it makes me happy and I plan on adding new features to
it as I get more time. >:3 If you'd like to help, feel free to contribute
and/or make a fork, pull request, etc. 

If you'd like to report any bugs, use the Issue Tracker, which I might put
up when I have time or send me an email at mooncake[at]hush[dot]com.


-----
Special Thanks
-----

To the Socially Encrypted Group :3 Sexiest people on the planet right there.
Feel free to come and chat in the IRC channel :3 kookie is usually there
herp derping around xD

Server: irc.sociallyencrypted.com
Port: 6667
Channel: #se

kookie was literally born in that channel. She lives and breathes there.

To StreetMentioner, for suggesting .isup, .smile and for inspiring me with
Mr "b0t" to create kookie <3

To the other sexeh kids in the group,
Wazakindjes, cM, Thalheim and anyone else who randomly tried out
kookie :3 <3

To shellium.org for getting me started on the bot.

To randominsults.net for having a site that generates insults xD

To isup.me for having a simple site that I could actually make use of xD




